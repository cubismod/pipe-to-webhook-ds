/**
Copyright 2021 cubis
Usage: pipe into this program to send piped data to a discord webhook
Based on https://flaviocopes.com/go-shell-pipes/
.env file includes one variable
WEBHOOK=discord_webhook_url
*/
package main

import (
	"bufio"
	"fmt"
	"io"
	"log"
	"os"
	"time"

	"github.com/DisgoOrg/disgohook"
	"github.com/DisgoOrg/disgohook/api"
	"github.com/joho/godotenv"
)


func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Unable to load .env file")
	}
	webhookUrl := os.Getenv("WEBHOOK")
	if webhookUrl == "" {
		log.Fatal("Unable to load WEBHOOK variable from .env file, please specify Discord webhook")
	}

	reader := bufio.NewReader(os.Stdin)
	
	// run a ticker to check every 2 seconds to see whats in the rune
	// buffer and then send a message to the webhook if its a newline
	ticker := time.NewTicker(2 * time.Second)
	runeChan := make(chan rune) // to pass a rune to separate goroutine
	flush := make(chan bool)	// indicates to the goroutine that it's time to flush
	// out whatever we have saved and send it

	// runs each tick
	go func() {
		webhook, err := disgohook.NewWebhookClientByToken(nil, nil, webhookUrl)
		if err != nil {
			log.Fatal(err)
		}
		var curRunes []rune	// where we store
		for {	
			select {
			case received := <-runeChan:	// add onto curRunes arr as we receive vals from main program
				curRunes = append(curRunes, received)
			case<-ticker.C:
				runeIndex := len(curRunes) - 1
				if runeIndex > 0 && curRunes[runeIndex] == '\n' {
					// change newline char to `
					postWebhook(webhook, curRunes)
					// prep to reset then send msg to reset to main routine
					curRunes = nil
				
				}
			case<-flush:
				// sure we're making some repetitive code but we're also not checking
				// length bounds every time the for loop runs
				runeIndex := len(curRunes) - 1
				if runeIndex > 0 && curRunes[runeIndex] == '\n' {
					postWebhook(webhook, curRunes)
				}
			}
		}
	}()

	// start off by sending a tilde to chan
	for {
		// read one rune at a time
		input, _, err := reader.ReadRune()

		if err != nil {
			flush<-true
			if err != io.EOF {
				log.Fatal(err)
			}
			
		} else {
			runeChan<-input	//otherwise send input to goroutine	
		}
	}	
}

// send a POST with the message to the webhook
func postWebhook(webhook api.WebhookClient, msg []rune) {
	// convert our rune array into a string
	// don't send just empty newline msgs as well
	str := "```\n" + string(msg) + "\n```"
	if str != "``" {
		_, err := webhook.SendEmbeds(api.NewEmbedBuilder().
		SetDescription(str).
		SetColor(7130790).
		SetTitle(fmt.Sprintf("<t:%d>", time.Now().Unix())).
		Build())
		if err != nil {
			log.Fatal(err)
		}
		time.Sleep(5 * time.Second)	// avoid ratelimits
	}
}